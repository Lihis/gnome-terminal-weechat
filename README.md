# GNOME Terminal WeeChat

WeeChat in unique GNOME Terminal instance.

## Build / install

Install dependencies:

- CMake (make)
- GNOME Terminal
- WeeChat

and then "build" and install:

```bash
cmake -B build -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_SYSCONFDIR=/etc
make -C build install
```

## Terminal customization

Configuration file `gnome-weechat.conf` (searched from `XDG_CONFIG_HOME` or
`${HOME}/.config`) can be used to customize terminal command line options.

```bash
GEOMETRY=100x55+10+10 # Argument: --geometry, COLSxROWS+X+Y
```

